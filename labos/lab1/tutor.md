# Git tuttorijal

```
git status 
```

Prikazuje trenutno stanje repozitorija

```
git commit -m "PORUKA"
```
Dodavanje poruke koje se prikazuje uz promjenu

```
git log
```
Pokazuje lokalne commitiove

``` 
git add PATH
```
Dodaje fileove u stage

```
git push origin master
```
Dodavanje na gitlab

```
git pull origin master
```
preuzimanje sadržaja promjenjeg repozitorija

``` 
git clone GITLAB_PATH
```
Klonira postojećeg repozitorija


```
git reset
```
Uklanja fileove iz stage-a


