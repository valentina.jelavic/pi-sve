from query_handler_base import QueryHandlerBase
import random
import requests
import json

class WeatherHandler(QueryHandlerBase):
 def can_process_query(self, query):
  if "weather" in query:
   return True
  return False


def process(self, query):
  lat = 45.55
  lon = 18.69

  try:
   result = self.call_api(lat, lon)
   temp = result["timezone"]
   self.ui.say(f"Timezone at current location is: {temp}")
  except:
    self.ui.say("Try something else!")


def call_api(self, lat, lon):
  url = "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/3hourly"

  querystring = {"lat":lat,"lon":lon}
  headers = {

     "X-RapidAPI-Key": "d7229d0acfmshffe89c6a1f35740p12e541jsn7196e8073165",
     "X-RapidAPI-Host": "weatherbit-v1-mashape.p.rapidapi.com"
  }
  response = requests.request("GET", url, headers=headers, params=querystring)

  return json.loads(response.text)