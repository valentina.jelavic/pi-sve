# LAB 6

## start a new project

git clone https://gitlab.com/elena.lucic/se-labs-2223-prp.git
ls
cd se-labs-2223-prp/
git config user.name elucic
git config user.email elucic@etfos.hr
git remote add upstream https://gitlab.com/levara/se-labs-2223-prp
git fetch upstream master
git merge upstream/master
mkdir lab6
ls
cd lab6
python -m venv env
source env/Scripts/activate
django-admin startproject myimgur
ls
cd myimgur
python manage.py startapp app
ls
code .
python manage.py migrate
winpty python manage.py runserver
winpty python manage.py createsuperuser
winpty python manage.py runserver
git status
cd ..
git status
ls
git add myimgur
git status
code ..
git status
git add ../.gitignore
git commit -m "add initial lab6"


